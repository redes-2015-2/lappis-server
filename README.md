# Lappis Server

## Linguagem/Dependências

Esse servidor foi escrito utilizando a linguagem ruby.

Caso você não tenha o ruby instalado em sua máquina basta fazer:

  - sudo apt-get install ruby ruby-dev
  - A versão do ruby utilizada para a construção do servidor foi a _2.1_, mas
  aparentemente não há amarrações feitas a versão do ruby, 
  portanto a versão que vier padrão no sistema operacional deve servir.

## Ambiente

- O servidor foi desenvolvido utilizando o sistema operacional *Debian testing (9)*
- O ambiente de desenvolvimento utilizado:
  - Editor: Vim
  - Terminal de comandos do sistema operacional
  - Browser: Iceweasel(um fork do firefox)
 

## Rodando o server
Para executar o servidor basta executar no terminal:

  - ruby server.rb

Para instruções como mudança de porta e endereço:

  - ruby server.rb --help


Para visualizar o response retornado pelo servidor:
  - curl -I 127.0.0.1:12000/index.html: Isso funcionará se você iniciar o 
  servidor sem passar nenhuma paramatro, apenas com ruby server.rb. 
  Caso você mude a porta e o endereço, utilize-os ao executar o curl.
  - Para visualizar o conteúdo do arquivo index.html basta remove a flag -I da
    chamada do curl.
  - Você pode realizar a mesma requisição via Browser também.


## Limitações

O funcionamento do servidor é bastante simples, ele irá retornar o conteúdo
de um arquivo html chamado index.html,
caso seja feita uma requisição para o mesmo.
