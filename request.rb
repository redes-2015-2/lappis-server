class Request

  attr_accessor :type

  def initialize type
    self.type = check_requisition(type)
  end


  # client_request: "GET /index.html HTTP/1.1"
  def extract_requested_object client_request
    clean_object_string(client_request.split(" ")[1])
  end

  private

  def clean_object_string object
    obj_length = object.length
    new_object = object[1..obj_length]
    new_object
  end

  def check_requisition type
    return 'GET' if type  =~/GET/
    nil
  end
end
