require 'optparse'

Options = Struct.new(:name)

class ArgParser
  attr_accessor :server

  def initialize(server)
    self.server = server
  end

  def parse(options)
    args = Options.new("server")

    opt_parser = OptionParser.new do |opts|
      opts.banner = "Usage: server.rb [options]"

      opts.on("-pPORT", "--port=PORT", "Server port") do |port|
        self.server.port = port
      end

      opts.on("-aHOST", "--address=HOST", "Server address") do |address|
        self.server.address = address
      end

      opts.on("-h", "--help", "Prints this help") do
        puts opts
        exit
      end
    end

    opt_parser.parse!(options)
    return args
  end
end
