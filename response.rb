class  Response

  attr_accessor :object_requested

  def response_not_found
    "HTTP/1.1 404 Not Found\r\n" +
    "Content-Type: text/plain\r\n" +
    "Content-Length: 0 \r\n" +
    "Connection: close\r\n" +
    "\r\n"
  end

  def response_ok object
    "HTTP/1.1 200 OK\r\n" +
    "Connection: close\r\n" +
    "Accept-Ranges: bytes\r\n" +
    "Content-Type: #{object_type}\r\n" +
    "Content-Length: #{object.size}\r\n" +
    "\r\n"
  end

  def get_requested_object requested_object
    files = Dir.entries('files/')
    files.delete('.')
    files.delete('..')
    requested_file = if files.include? requested_object
                        self.object_requested = requested_object
                        File.open('files/' + requested_object.to_s, 'rb')
                     else
                       nil
                     end
    requested_file
  end

  private

  # recupera a extensao do objeto requisitado
  def object_type
   self.object_requested.split(".")[1]
  end
end
