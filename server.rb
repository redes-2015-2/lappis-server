require 'socket'
load 'arg_parser.rb'
load 'request.rb'
load 'response.rb'

class Server
  attr_accessor :server, :port, :address

  def initialize
    self.port = 12000
    self.address = '127.0.0.1'
  end

  def start_server
    begin
      self.server = TCPServer.new(self.address, self.port)
      puts "Listening at #{self.address}:#{self.port}"
    rescue Errno::EADDRINUSE
      abort('Port unavailable.')
    end
  end

  def run()
    loop do
      # Aguarda por uma conexão co cliente
      client = self.server.accept

      #Le linha a linha da requisição feita pelo client.
      client_request = client.gets

      # Request e Response sao responsaveis tanto pelo parseamento da requisicao
      # quanto pelo envio ao cliente do objeto solicitado.
      request = Request.new(client_request)
      response = Response.new

      STDERR.puts 'REQUISITION TYPE: GET'

      #Parseamento da Requisicao
      requested_object = request.extract_requested_object(client_request)
      object = response.get_requested_object(requested_object)

      # Caso o objeto requisitado exista, o enviamos para o cliente
      # e fechamos a conexao
      if object
        client.print response.response_ok(object)
        IO.copy_stream(object, client)
        object.close
      else
        client.print response.response_not_found
      end

      client.close
    end
  end
end

parser = ArgParser.new(Server.new)
option = parser.parse ARGV
parser.server.start_server
parser.server.run
